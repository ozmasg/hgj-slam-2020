﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class GameData
{
  public class Items
  {
    public const string whiteTape = "whitetape";
    public const string redTape = "redtape";
    public const string food = "food";
  }

  public enum Events
  {
    NoEvent = 0,

    AdvanceLoop,

    BuddyGoingToToilet,

    PlayerEnteredToilet,
    PlayerEnteredBunk,

    PickedUpWhiteTape,
    PickedUpRedTape,
    PickedUpFood,

    UseWhiteTapeOnBananaTree,
    UseRedTapeOnBananaTree,

    PlaceFoodOnRedPlate,
    PlaceFoodOnWhitePlate,

    ChooseEnglishName,
    ChooseChineseName,

    SpeakToBuddy,

    SpeakToSentry2InLoop1,
    SpeakToSgtInLoop1AfterSpeakToSentry2,
    SpeakToSentry1InLoop2,

    Endgame,
    Closegame,
  }

  public class Dialogue
  {
    public const string playerID = "player";
    public const string buddyID = "buddy";
    public const string sentry1ID = "sentry1";
    public const string sentry2ID = "sentry2";
    public const string sgtID = "sgt";

    private static readonly DialogueChoice exitConversation = new DialogueChoice
    {
      optionText = "Leave",
    };

    /************************************
     * 
     * 
     * LOOP 0 DIALOGUES
     * 
     * 
     * *********************************/

    //  Initial (walking) conversation with buddy to sentry 1 post

    public static readonly DialogueSet loop0Buddy1Initial = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Eh you awake yet anot?",
          "Take so long to put on uniform. We gotta start already eh.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Sorry lah, I was smoking.",
        }},
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Everyday smoke wahlau, one day cannot no smoke meh?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Can is can lah, but got nothing to do so just smoke right?",
        }},
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Now you got something to do.",
          "Your first patrol right?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah. What do I need to do?",
        }},
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Basically, we walk around the camp just in case got something happen.",
          "So we need to check in at 3 places.",
          "The guardhouse, where the sergeant is standing over there.",
          "And the two other sentries will have books for us to sign.",
          "Follow me, I'll show you where they are."
        }},
      },
      triggeredEvent = GameData.Events.SpeakToBuddy,
    };
    public static readonly DialogueSet loop0Buddy1AfterSpeakingToS1 = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Sign already anot?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah, just signed.",
        }},
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "You know I heard they last time here got banana tree.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Dude, you and Bryan trying to scare me issit?",
        }},
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Whattt no lah... I not so bad one.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Aiya to be honest, everyone always talk about pontianak. What is it anyway?",
        }},
      },
      triggeredEvent = GameData.Events.SpeakToBuddy,
    };


    public static readonly DialogueSet loop0Buddy1HasntSpokenToS1 = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "What are you waiting for? Go sign the book.",
        }},
      }
    };
    public static readonly DialogueSet loop0Buddy1HasntSpokenToS2 = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=buddyID, text= new List<string>{
          "Did you ask him about the pontianak?",
        }},
      }
    };
    public static readonly DialogueSet loop0Buddy1GoingToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Eh I'm going to the toilet. You go guard house first can?",
          "It's that small building behind the armoury.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Where is the armoury?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Aiya, where else-",
          "...",
          "Just go to the small building behind this big gray one, bro.",
        }},
      },
      triggeredEvent = GameData.Events.BuddyGoingToToilet,
    };

    public static readonly DialogueSet loop0Sentry1EngNameDSet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Hey Randall. First patrol right?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Ya, must sign something right?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Yup, let me get the book.",
          "Just need to put your name and time.",
          "Yeah, just sign there.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay, all done. Thanks bro.",
          "See you later.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Aight. See you later, be careful ah.",
          "It's the 7th month.",
        }},
      }
    };
    public static readonly DialogueSet loop0Sentry1ChiNameDSet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Aiya call me Bryan, your chinese sucks.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Haha I know lah",
          "Anyway it's my first patrol, I need to sign something issit?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Yeah let me get the book.",
          "Just sign here with your name and time.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "K thanks bro.",
          "Okay and...",
          "Done.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Aight. See you later, be careful ah.",
          "It's the 7th month.",
        }},
      }
    };
    public static readonly DialogueChoiceSet loop0Sentry1Choice = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{ optionText="Yo, Bryan", dialogue = loop0Sentry1EngNameDSet, triggeredEvent=GameData.Events.ChooseEnglishName },
        new DialogueChoice{ optionText="Yo, Lim Qian", dialogue = loop0Sentry1ChiNameDSet,triggeredEvent=GameData.Events.ChooseChineseName }
      }
    };
    public static readonly DialogueSet loop0Sentry1AfterBuddyGoesToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "If you're done with this patrol, you should go sign the book.",
          "Where? Walao eh really blur ah you.",
          "Should be inside the guard house, back where you started."
        }},
      }
    };


    public static readonly DialogueSet loop0Sentry2InitDefault = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yo Jun.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Hey Randall. First patrol?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah, sign here right?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Yeah bro.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay... Signed.",
          "Eh I ask you ah, pontianak is what ah?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "DUDE!",
          "This thing cannot anyhow ask lah!",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Aiya, ask only one.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "It's like uh... a lady with long hair, white dress, and eats men that kind of stuff.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Oh okay lah. That's not that scary.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Bro if you wanna talk about stuff like this, please go somewhere else.",
          "Far away from me, okay?",
        }},
      }
    };


    public static readonly DialogueSet loop0SgtDefault = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Hey Randall, you finishing your first round?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yea sargen, do I need to sign something here also?",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Yup, the book's in there.",
          "Just need your name, rank, and the time now.",
        }},
      }
    };

    public static readonly DialogueSet loop0SgtHasntGoneToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Are you speedrunning this game?",
        }},
      }
    };

    public static readonly DialogueSet loop0SgtAfterSigningBook = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Wasn't Nicholas patrolling with you? Where he go?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Toilet.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "He didn't go back to sleep and ask you patrol yourself right?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "No lah, he just needed to go toilet.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Ok, ok if you say so.",
          "When you go back to patrol, go find him first ok.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yup. See you later sarge.",
        }},
      },
      triggeredEvent = GameData.Events.AdvanceLoop,
    };

    public static readonly DialogueChoiceSet loop0SignBookChoice = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Sign the book (proceed to next patrol).",
          dialogue = loop0SgtAfterSigningBook
        },
        new DialogueChoice{ optionText="Maybe later." }
      }
    };

    /************************************
     * 
     * 
     * LOOP 1 DIALOGUES
     * 
     * 
     * *********************************/

    public static readonly DialogueSet loop1Sentry1TalkAboutRT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What do I need to do with the red tape again?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Just tie it around the banana tree.",
          "Make sure to tie it when the moonlight is shining on you.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Got so specific one meh?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Yeah, these things usually my ahma or ahkong got instruction manual one.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry1TalkAboutWT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What do I need to do with the white tape again?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "White tape? I don't know, tie around your finger.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "My finger? Why?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "So that you remember it's supposed to be red tape you bodoh.",
        }},
      }
    };

    private static readonly DialogueChoice l1S1TalkAboutRedTape = new DialogueChoice
    {
      optionText = "Ask about red tape",
      dialogue = loop1Sentry1TalkAboutRT
    };
    private static readonly DialogueChoice l1S1TalkAboutWhiteTape = new DialogueChoice
    {
      optionText = "Ask about white tape",
      dialogue = loop1Sentry1TalkAboutWT
    };

    public static readonly DialogueSet loop1Sentry1Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yo Bryan have you seen Nic?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Weren't you patrolling together?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Ya, then he went toilet and didn't come back.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "The moon's quite bright tonight isn't it?",
          "I've heard that the pontianak comes out during nights like these.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What? Don't lame eh.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "I'm just saying man.",
          "If there really is a pontianak tho, you should find some red tape."
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Red tape? Why?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "My ahkong used to tell me that if you tie the red tape on a banana tree,",
          "then pontianak will stay away."
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Hmm. I think I saw some red tape in the toilet.",
        }},
      }
    };
    public static readonly DialogueChoiceSet loop1Sentry1HasRTHasWT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S1TalkAboutRedTape,
        l1S1TalkAboutWhiteTape,
        exitConversation,
      }
    };
    public static readonly DialogueChoiceSet loop1Sentry1HasRT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S1TalkAboutRedTape,
        exitConversation,
      }
    };
    public static readonly DialogueChoiceSet loop1Sentry1HasWT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S1TalkAboutWhiteTape,
        exitConversation,
      }
    };
    public static readonly DialogueSet loop1Sentry1AfterTyingRT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I tied it liao.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Tied what?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "The red tape on the banana tree lah.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Oh yeah.",
          "That's good lor.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry1AfterTyingWT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I tied it liao.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Tied what?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "The white tape on the banana tree lah.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Oh okay good.",
          "Wait, the white tape?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah, why?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "It's supposed to be red tape dude!",
          "Siannnnn...",
          "You've doomed us all.",
        }},
      }
    };

    public static readonly DialogueSet loop1Sentry2Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yo Jun have you seen Nic?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Nope. Nothing to see here.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Dammit. He went toilet then haven't come back. Maybe he's slacking off in the bunk.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Or he kena the pontianak.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What? I thought you said not to mention it!",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Yeah, but after you said it, it it's all I can think of!",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Whatever, there's no banana tree around here anyway.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "There is, near the toilet. Also, can't you smell the frangipani?",
          "Better get white tape just in case.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "White tape? For what?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "My mum told me that you need to tie white tape around the banana tree",
          "To ward away the pontianak... or capture it, can't remember.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Well... I think the sergeant has some white tape.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry2TalkAboutRT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What do I need to do with the red tape again?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Red tape? The fate one or gahmen one?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Nono, the real physical one.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "It's not red tape bro, it's white tape.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry2TalkAboutWT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What do I need to do with the white tape again?",
        }},

        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "You need to tie it around the banana tree",
          "where it's trunk is the thickest."
        }},
         new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Wow, I need to measure that?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Just agar agar lah.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry2AfterTyingRT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I tied the red tape already.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Red tape? What red tape?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "The one around the banana tree.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Abang, it's supposed to be white tape man.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Oh crap, then how?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Mampus.",
          "Pray lah.",
        }},
      }
    };
    public static readonly DialogueSet loop1Sentry2AfterTyingWT = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I tied the white tape already.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Around the banana tree girth?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Uhhh... yeah. At least I think so.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Okay can relack already. No more trouble tonight.",
        }},
      }
    };

    private static readonly DialogueChoice l1S2TalkAboutRedTape = new DialogueChoice
    {
      optionText = "Ask about red tape",
      dialogue = loop1Sentry2TalkAboutRT,
    };
    private static readonly DialogueChoice l1S2TalkAboutWhiteTape = new DialogueChoice
    {
      optionText = "Ask about white tape",
      dialogue = loop1Sentry2TalkAboutWT
    };
    public static readonly DialogueChoiceSet loop1Sentry2HasRTHasWT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S2TalkAboutRedTape,
        l1S2TalkAboutWhiteTape,
        exitConversation,
      }
    };
    public static readonly DialogueChoiceSet loop1Sentry2HasRT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S2TalkAboutRedTape,
        exitConversation,
      }
    };
    public static readonly DialogueChoiceSet loop1Sentry2HasWT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        l1S2TalkAboutWhiteTape,
        exitConversation,
      }
    };

    public static readonly DialogueSet loop1MonoEnteringToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Bro are you there?",
          "Nic don't scare me leh.",
        }},
      },
      triggeredEvent = GameData.Events.PlayerEnteredToilet
    };
    public static readonly DialogueSet loop1MonoExitingToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Where he go sia?",
          "Maybe Bryan or Jun saw him.",
        }},
      },
    };
    public static readonly DialogueSet loop1MonoPickupRedTape = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "It's some red tape.",
        }},
      }
    };
    public static readonly DialogueSet loop1MonoPickupWhiteTape = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "It's some white tape.",
        }},
      }
    };
    public static readonly DialogueSet loop1MonoInteractTreeWithoutTape = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "It's a banana tree.",
          "It casts an imposing shadow.",
        }},
      }
    };
    public static readonly DialogueSet loop1MonoPlaceRedTape = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "You can hardly even see the red tape in this darkness.",
          "I should return to my patrol.",
        }},
      }
    };
    public static readonly DialogueSet loop1MonoPlaceWhiteTape = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "The white tape really stands out on the banana tree.",
          "I should return to my patrol."
        }},
      }
    };

    public static readonly DialogueSet loop1SgtInteractBeforePlayerGoToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Have you checked the toilet?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Not yet.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Waiting for Christmas ah?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "I'll go check now.",
        }},
      }
    };
    public static readonly DialogueSet loop1SgtDefault = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Still can't find Nicholas?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah I checked the toilet but he wasn't there.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Probably still slacking somewhere.",
          "Did you sign the other books for your patrol?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Uhm... yeah. Yeah I did.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Okay, remember to sign the one here too.",
        }},
      }
    };
    public static readonly DialogueSet loop1SgtAfterTalkingToSentry2 = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Hey sarge, do you have any white tape?",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Yeah, what do you need it for?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Hunting a hantu.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "I'm not even going to ask.",
          "Just don't take too much. I need it to mark toilet area for tomorrow's range.",
        }},
      },
      triggeredEvent = GameData.Events.SpeakToSgtInLoop1AfterSpeakToSentry2,
    };
    public static readonly DialogueSet loop1SgtPlayerHasntGoneToilet = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "You're still here? Go do your patrol lah.",
          "You need to check in with both sentries.",
        }},
      }
    };
    public static readonly DialogueSet loop1SgtAfterSigningBook = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "The night feels quieter somehow.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What do you mean?",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Don't know, just feels like it.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Maybe because Nic not around.",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Check the bunk lah, maybe he's slacking off there.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah you're right. See you later.",
        }},
      },
      triggeredEvent = GameData.Events.AdvanceLoop,
    };
    public static readonly DialogueChoiceSet loop1SignBookChoiceNotReady = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Sign the book (proceed to next patrol).",
          dialogue = loop1SgtPlayerHasntGoneToilet
        },
        new DialogueChoice{ optionText="Maybe later (continue exploring)." }
      }
    };
    public static readonly DialogueChoiceSet loop1SignBookChoiceReady = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Sign the book (proceed to next patrol).",
          dialogue = loop1SgtAfterSigningBook
        },
        new DialogueChoice{ optionText="Maybe later (continue exploring)." }
      }
    };

    /************************************
     * 
     * 
     * LOOP 2 DIALOGUES
     * 
     * 
     * *********************************/

    public static readonly DialogueSet loop2Sentry1Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Still haven't found Nic?",
        }},

        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah. He wasn't in the bunk.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Eh maybe he got taken.",
        }},

        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Taken? By what?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Hungry ghost lah! Nobody put the offering so maybe it took someone.",
        }},

        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "You didn't put?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "No, but I know somebody put out the plates behind the armoury.",
        }},

        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay fine I'll put it.",
          "Can use the night snack for hungry ghost offering right?",
          "I think there should be some in the bunk...",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Should be can lah, but remember to put the offering on the red plate.",
          "That's how my mum always puts it.",
        }},
      },
      triggeredEvent = GameData.Events.SpeakToSentry1InLoop2,
    };
    public static readonly DialogueSet loop2Sentry1TalkAboutFood = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Which plate to put the food again?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "The red plate man. Can hurry anot?",
          "I think I heard something just now.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What did you hear?",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Dude, just ka kin put the food on the red plate.",
        }},
      }
    };
    public static readonly DialogueSet loop2Sentry1TalkAboutFoodOnRedPlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I put the offering onto the red plate.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Heng ah. We should be fine for now.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Nic is still missing though... I should find him after I finish patrolling.",
        }},
      }
    };
    public static readonly DialogueSet loop2Sentry1TalkAboutFoodOnWhitePlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I put the offering onto the white plate.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "White plate? It's the red plate lah!",
          "Go back and change pls.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Siao ah, asking me to disturb the offering.",
        }},
        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Sian you're right. You disturb it wait they eat you instead.",
          "Better hope nothing happens to you man."
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah, yeah. I just wanna find Nic and get this patrol done with.",
        }},

      }
    };

    public static readonly DialogueSet loop2Sentry2Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Still patrolling solo?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Yeah, just checked the bunk. No idea where he went.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Maybe its the hungry ghost. It eats people if no offering right?",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Touch wood lah!",
          "Since when does hungry ghost eat people?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Wah I Malay how come I know more than you about this?",
          "Hungry ghost will eat people if you don't offer food on a white plate.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Wah power sia, how come you know?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "My girlfriend Chinese what. She teach me abit from her ah ma.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Aiya alright I go put lah. Everything I do.",
          "Can use the night snack for this right?"
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Yeah, yeah. I think you can find some in the bunk.",
          "Oh ya, I saw Nic putting the plates behind the armoury earlier tonight.",
        }},
      },
    };
    public static readonly DialogueSet loop2Sentry2TalkAboutFood = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Which plate to put the food on again ah?",
        }},

        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "The white one. But faster abit can anot?",
          "I think if you do it after a certain time it'll be too late.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Too late for what?",
        }},

        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Just go lah!",
          "Cepat cepat!",
        }},
      }
    };
    public static readonly DialogueSet loop2Sentry2TalkAboutFoodOnRedPlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I put the offering onto the red plate.",
        }},

        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Huh? Why red bro, it's supposed to be the white plate.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Wait I thought you said red?",
        }},

        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Astaghfirullah...",
          "Never mind lah, Nic die only.",
        }},
      }
    };
    public static readonly DialogueSet loop2Sentry2TalkAboutFoodOnWhitePlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Okay I put the offering onto the white plate.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Bagus lah. Maybe Nic went back to the guardhouse by now.",
          "Go check in with the sergeant lah."
        }},
      }
    };

    public static readonly DialogueSet loop2MonoSignBookBeforeTalkingToSentries = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "I should probably check in with both of the guys.",
        }},
      },
    };
    public static readonly DialogueSet loop2MonoEnterBunk = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Nic you freakin slacker!",
          "We're supposed to be patrolling leh",
          "Where is he?",
          "...",
          "I should ask around.",
        }},
      },
      triggeredEvent = GameData.Events.PlayerEnteredBunk,
    };
    public static readonly DialogueSet loop2MonoInteractPlateWithoutFood = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Offering plates for the 7th month.",
          "They look strange without food.",
        }},
      }
    };
    public static readonly DialogueSet loop2MonoPickupFood = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Apollo pandan cake. Huh. I used to eat a ton of these when I was a kid.",
        }},
      }
    };
    public static readonly DialogueSet loop2MonoPlaceFoodOnRedPlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Alright, that's done. I'd better get back to my patrol.",
        }},
      }
    };
    public static readonly DialogueSet loop2MonoPlaceFoodOnWhitePlate = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Alright, that's done. I'd better get back to my patrol.",
        }},
      }
    };

    public static readonly DialogueSet loop2SgtDefault = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "And so she hung herself from the banana tree.",
          "The white tape now stained with blood.",
          "Or perhaps the tape had always been red.",
          "The blood of the stillborn child she was birthed from.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Sergeant?",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Oh, sorry. I uh... just remembered something.",
          "Didn't know you were still there.",
        }},
      }
    };
    public static readonly DialogueSet loop2SgtSignBookBeforeGoingBunk = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
          "Oy, go back patrolling lah!",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Ah sorry sergeant!",
        }},
        new DialogueEntry{ speaker=sgtID, text= new List<string>{
         "Remember to check the bunk for Nicholas ah!",
        }},
      }
    };

    public static readonly DialogueSet loop2MonoAfterSigningBook = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Where did sarge go? I didn't see him leave at all...",
        }},
      },
      triggeredEvent = GameData.Events.AdvanceLoop,
    };

    public static readonly DialogueChoiceSet loop2SignBookChoiceNotReady = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Sign the book (proceed to next patrol).",
          dialogue = loop2SgtSignBookBeforeGoingBunk
        },
        new DialogueChoice{ optionText="Maybe later (continue exploring)." }
      }
    };
    public static readonly DialogueChoiceSet loop2SignBookChoiceReady = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Sign the book (proceed to next patrol).",
          dialogue = loop2MonoAfterSigningBook,
        },
        new DialogueChoice{ optionText="Maybe later (continue exploring)." }
      }
    };

    /************************************
     * 
     * 
     * LOOP 3 DIALOGUES
     * 
     * 
     * *********************************/

    public static readonly DialogueSet loop3Sentry1Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Sergeant left also sia, did you see him?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Haven't seen anyone for awhile. The lights keep going off eh, hard to see.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "What? But why though?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "I don't know man... Today sibei strange night.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Are all patrols like this?",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "No... usually there aren't weird noises...",
          "...or white figures standing in the distance.",
          "Also I'm pretty sure there was no banana tree in this entire campe.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "I... I think I'm gonna check the bunk out once more.",
        }},

        new DialogueEntry{ speaker=sentry1ID, text= new List<string>{
          "Okay, can you send out my replacement also?",
          "I don't want to be outside anymore."
        }},
      }
    };

    public static readonly DialogueSet loop3Sentry2Init = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Jun, did you see the sergeant?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "I can't really see any of y'all from over here.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Oh, yeah. This armoury is damn big.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "Yeah although sometimes i'll see one of our bunk mates walking over there.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Over where?",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "That side.",
        }},
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "Bro I've been out all night and I haven't seen anyone else.",
        }},
        new DialogueEntry{ speaker=sentry2ID, text= new List<string>{
          "...",
          "Can you help me ask for my replacement?",
          "I think my shift just ended.",
        }},
      }
    };

    public static readonly DialogueSet loop3MonoSignBook = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry{ speaker=playerID, text= new List<string>{
          "I think I'm done with the patrol.",
          "Should head back to the bunk.",
        }},
      }
    };

    /************************************
    * 
    * 
    * LOOP 3 DIALOGUES
    * 
    * 
    * *********************************/

    public static readonly DialogueSet goodEndDialogue = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Nic? Are you there?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Yeah wassup dude.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "What the...",
          "What do you mean 'yeah wassup dude'",
          "Have you been here this entire time??",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Haha yeah, I expected you to find me faster.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "But I checked the toilet and the bunk??",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Yeah I saw you both times, but you were mumbling to yourself and looking desperately for some stuff.",
          "I thought it'd be funny to just not do anything.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "What the hell man I was seriously worried.",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Aiya chill lah, guard duty is just damn boring -",
          "gotta entertain ourselves somehow.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Ourselves?",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "Haha yeah sorry man, I knew the entire time.",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "Same.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
         "Wuh-Buh- Bryan?? Jun??",
         "Where y'all appear from sia?",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "Cos you blur sotong. We just walked pass you.",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "Yeah I even saw you putting night snack on the floor just now.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "But-But-",
          "The 7th month...",
          "A-and the banana tree!",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "No banana tree lah bro, you just tied the tape to a random angsana.",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "Yeah and if I were a hungry ghost,",
          "I sure won't eat Apollo pandan cake.",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "You put out Apollo pandan cake as an offering?",
          "Bro you champion lah.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Y'all said it would be fine!",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "To be honest, I thought you'd know more about these things",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Why would I know though?",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "More like, everyone knows right?",
          "Every year got 7th month what.",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "Yeah and if you can find one Singaporean that doesn't know about pontianak",
          "I buy you lunch.",
          "Confirm someone talk to you about it before.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "...",
          "Say until like that...",
          "Tell me more eh, since I don't know.",
          "Like, why is it the 7th month? And why banana tree, why angsana is nothing?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Wah respect sia. So scared then now asking for more stories.",
        }},
      },
      triggeredEvent = GameData.Events.Closegame,
    };

    public static readonly DialogueSet badEndDialogue = new DialogueSet
    {
      set = new List<DialogueEntry>
      {
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Randall? Is that you?",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Nic? Have you been here all this time?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Well, not the whole time.",
          "I went to the toilet, i guess.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Yeah but what about the patrol?",
          "I was looking for you sia!",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Oh yeah, sorry man I was going to get back to patrol but",
          "...",
          "I saw some weird stuff so I just came back to the bunk."
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "What... what kind of weird stuff?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Like... a banana tree suddenly appeared!",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "That's it? Then you ran back here??",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "Well - yeah! That's scary as hell! You not scared meh?",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Bro I've been seeing scary things the whole night",
          "Like I'm pretty sure I heard someone else walking near me,",
          "But I look around, nobody else sia.",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "Dude.",
          "Same.",
          "That really freaked me out.",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "And did you see that white figure?",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "GUYS!",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "You saw it too?!",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "The lights also sia!",
          "Why did they turn off??",
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "What was UP with the lights leh?",
          "Damn weird!",
        }},
        new DialogueEntry { speaker = sgtID, text = new List<string> {
          "What the hell is going on here?",
          "So damn noisy so late at night!",
          "Aren't you guys supposed to be on duty?"
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "Uhh...",
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "We uhh...",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "Sorry sarge, we saw some weird stuff so -",
        }},
        new DialogueEntry { speaker = sgtID, text = new List<string> {
          "It's okay. No need tell me.",
          "I needed some volunteers for this weekend anyway.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Wah sarge please eh."
        }},
        new DialogueEntry { speaker = sgtID, text = new List<string> {
          "Eh, I leave for a few hours and come back...",
          "And all my men slacking.",
          "Comfirm y'all kena sign extra lah.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "Wait, what? But you've been at the guard house.",
        }},
        new DialogueEntry { speaker = sgtID, text = new List<string> {
          "Siao ah.",
          "I went for a meeting somewhere else.",
        }},
        new DialogueEntry { speaker = playerID, text = new List<string> {
          "..."
        }},
        new DialogueEntry { speaker = buddyID, text = new List<string> {
          "...",
        }},
        new DialogueEntry { speaker = sentry1ID, text = new List<string> {
          "..."
        }},
        new DialogueEntry { speaker = sentry2ID, text = new List<string> {
          "...",
        }},
      },
      triggeredEvent = GameData.Events.Closegame,
    };

    /************************************
     * 
     * 
     * INTERACTABLE CHOICE DIALOGUES
     * 
     * 
     * *********************************/

    public static readonly DialogueChoiceSet foodWithPlateChoices = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Place the food on the red plate",
          triggeredEvent=GameData.Events.PlaceFoodOnRedPlate,
          dialogue = GameData.Dialogue.loop2MonoPlaceFoodOnRedPlate,
        },
        new DialogueChoice{
          optionText="Place the food on the white plate",
          triggeredEvent=GameData.Events.PlaceFoodOnWhitePlate,
          dialogue = GameData.Dialogue.loop2MonoPlaceFoodOnWhitePlate,
        },
        new DialogueChoice{ optionText="Do nothing" }
      }
    };

    public static readonly DialogueChoiceSet bananaTreeChoiceHRT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Use the red tape on the tree",
          triggeredEvent=GameData.Events.UseRedTapeOnBananaTree,
          dialogue=GameData.Dialogue.loop1MonoPlaceRedTape,
        },
        new DialogueChoice{ optionText="Do nothing" }
      }
    };
    public static readonly DialogueChoiceSet bananaTreeChoiceHWT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Use the white tape on the tree",
          triggeredEvent=GameData.Events.UseWhiteTapeOnBananaTree,
          dialogue=GameData.Dialogue.loop1MonoPlaceWhiteTape,
        },
        new DialogueChoice{ optionText="Do nothing" }
      }
    };

    public static readonly DialogueChoiceSet bananaTreeChoiceHWRT = new DialogueChoiceSet
    {
      choices = new List<DialogueChoice>
      {
        new DialogueChoice{
          optionText="Use the red tape on the tree",
          triggeredEvent=GameData.Events.UseRedTapeOnBananaTree,
          dialogue=GameData.Dialogue.loop1MonoPlaceRedTape,
        },
        new DialogueChoice{
          optionText="Use the white tape on the tree",
          triggeredEvent=GameData.Events.UseWhiteTapeOnBananaTree,
          dialogue=GameData.Dialogue.loop1MonoPlaceWhiteTape,
        },
        new DialogueChoice{ optionText="Do nothing" }
      }
    };
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugPrinter : MonoBehaviour
{
  public static DebugPrinter inst { get; private set; }
  public Text debugText;
  public float debugTextDisplayDuration = 3.0f;
  public float timer;

  public void Start()
  {
    inst = this;
  }

  public void OnDestroy()
  {
    inst = null;
  }

  public void Update()
  {
    if (timer <= 0)
      return;

    timer -= Time.deltaTime;
    if (timer <= 0)
    {
      timer = 0;
      debugText.gameObject.SetActive(false);
    }
  }

  public void PrintDebugText(string message)
  {
    timer = debugTextDisplayDuration;
    debugText.gameObject.SetActive(true);
    debugText.text = message;
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueEntry
{
  public string speaker;
  public List<string> text;
}

public class DialogueSet
{
  public List<DialogueEntry> set;
  public GameData.Events triggeredEvent = GameData.Events.NoEvent;

  public int Count
  {
    get => set.Count;
  }

  public DialogueEntry this[int key]
  {
    get => set[key];
  }
}

public class DialogueChoice
{
  public string optionText;
  //  Sent to event-handler class if this choice is chosen
  public GameData.Events triggeredEvent = GameData.Events.NoEvent;
  public DialogueSet dialogue;
}

public class DialogueChoiceSet
{
  public List<DialogueChoice> choices;
}

public class DialogueSystem : MonoBehaviour
{
  public static DialogueSystem inst { get; private set; }

  public PTPlayerController playerRef;
  public PTBuddyAI buddyRef;
  public PTSentryAI sentry1Ref;
  public PTSentryAI sentry2Ref;
  public PTSentryAI sgtRef;

  public SpeechBubble playerSBRef;
  public SpeechBubble buddySBRef;
  public SpeechBubble sentry1SBRef;
  public SpeechBubble sentry2SBRef;
  public SpeechBubble sgtSBRef;

  public Text choiceTextRef;

  public GameObject choicePanel;
  public GameObject choice1;
  public Text choiceText1;
  public GameObject choice2;
  public Text choiceText2;
  public GameObject choice3;
  public Text choiceText3;
  public GameObject choice4;
  public Text choiceText4;

  public bool isRunningDialogue;
  public bool isPendingChoice;
  public DialogueSet currDialogue;
  public DialogueChoiceSet currChoiceSet;
  public int currIndex;
  public int currEntryIndex;
  private SpeechBubble currSpeaker;

  public float updateTextInterval = 0.1f;
  private float timeSinceLastUpdate = 0.0f;

  public void Awake()
  {
    inst = this;
  }

  public void OnDestroy()
  {
    inst = null;
  }

  public void Update()
  {
    if (isRunningDialogue)
    {
      if (!UpdateDialogue())
      {
        timeSinceLastUpdate += Time.deltaTime;
      }
      else
      {
        timeSinceLastUpdate = 0;
      }
      return;
    }

    if (isPendingChoice)
    {
      CheckForChoiceInput();
      return;
    }
  }

  public bool UpdateDialogue()
  {
    bool interacting = Input.GetButton("Interact");

    if (!interacting)
    {
      return false;
    }

    if (timeSinceLastUpdate <= updateTextInterval)
    {
      return false;
    }

    //  Advance current dialogue if there are any left
    ++currEntryIndex;
    if (currDialogue[currIndex].text.Count != currEntryIndex)
    {
      currSpeaker.SetDialogue(currDialogue[currIndex].text[currEntryIndex]);
      return true;
    }

    //  Since there is no more dialogue for current speaker, we hide the bubble
    currSpeaker.SetVisibility(false);

    //  Check for next speaker
    ++currIndex;
    if (currDialogue.Count == currIndex)
    {
      GameData.Events eventToTrigger = currDialogue.triggeredEvent;
      StopDialogue();
      Reset();
      PTGameStateManager.TriggerEvent(eventToTrigger);
      return true;
    }

    //  Start the next speaker's text
    currEntryIndex = 0;
    currSpeaker = GetSpeechBubbleRef(currDialogue[currIndex].speaker);
    if (currSpeaker == null)
    {
      StopDialogue();
      Reset();
      return true;
    }

    currSpeaker.SetDialogue(currDialogue[currIndex].text[currEntryIndex]);
    return true;
  }

  public void StartDialogue(DialogueSet dialogue)
  {
    if (dialogue == null || dialogue.Count == 0)
    {
      Debug.LogError("Attempted to StartDialogue with a null/empty dialogue list");
      return;
    }

    Reset();
    SpeechBubble speaker = GetSpeechBubbleRef(dialogue[0].speaker);
    if (speaker == null)
      return;

    isRunningDialogue = true;
    isPendingChoice = false;
    currDialogue = dialogue;
    playerRef.TogglePlayerControl(false);
    speaker.SetDialogue(dialogue[0].text[0]);
    currSpeaker = speaker;
  }

  public void StartDialogueChoice(DialogueChoiceSet cs)
  {
    Reset();
    isPendingChoice = true;
    currChoiceSet = cs;
    ToggleChoiceObjects(false);

    choicePanel.gameObject.SetActive(true);

    for (int it = 0; it < cs.choices.Count; ++it)
    {
      GameObject objectToModify = null;
      Text textToModify = null;

      switch (it)
      {
        case 0:
          objectToModify = choice1;
          textToModify = choiceText1;
          break;
        case 1:
          objectToModify = choice2;
          textToModify = choiceText2;
          break;
        case 2:
          objectToModify = choice3;
          textToModify = choiceText3;
          break;
        case 3:
          objectToModify = choice4;
          textToModify = choiceText4;
          break;
        default:
          Debug.LogError("Too many dialogue choices found");
          return;
      }

      string ctext = "";
      ctext += (it + 1) + ": ";
      ctext += cs.choices[it].optionText;
      textToModify.text = ctext;
      objectToModify.SetActive(true);
    }

    playerRef.TogglePlayerControl(false);
  }

  public bool CheckForChoiceInput()
  {
    int inputChoice = -1;

    if (Input.GetKeyDown(KeyCode.Alpha1))
      inputChoice = 0;
    else if (Input.GetKeyDown(KeyCode.Alpha2))
      inputChoice = 1;
    else if (Input.GetKeyDown(KeyCode.Alpha3))
      inputChoice = 2;
    else if (Input.GetKeyDown(KeyCode.Alpha4))
      inputChoice = 3;
    else if (Input.GetKeyDown(KeyCode.Alpha5))
      inputChoice = 4;

    if (inputChoice == -1 || inputChoice >= currChoiceSet.choices.Count)
      return false;

    ResolveChoice(inputChoice);
    return true;
  }

  private void ToggleChoiceObjects(bool set)
  {
    choicePanel.gameObject.SetActive(set);
    choice1.gameObject.SetActive(set);
    choice2.gameObject.SetActive(set);
    choice3.gameObject.SetActive(set);
    choice4.gameObject.SetActive(set);
  }

  private void StopDialogue()
  {
    playerRef.TogglePlayerControl(true);
    if (currSpeaker != null)
      currSpeaker.SetVisibility(false);
  }

  private void Reset()
  {
    isRunningDialogue = false;
    isPendingChoice = false;
    currDialogue = null;
    currIndex = 0;
    currEntryIndex = 0;
    currSpeaker = null;
  }

  private SpeechBubble GetSpeechBubbleRef(string name)
  {
    switch (name)
    {
      case GameData.Dialogue.playerID:
        return playerSBRef;
      case GameData.Dialogue.buddyID:
        return buddySBRef;
      case GameData.Dialogue.sentry1ID:
        return sentry1SBRef;
      case GameData.Dialogue.sentry2ID:
        return sentry2SBRef;
      case GameData.Dialogue.sgtID:
        return sgtSBRef;
    }
    return null;
  }

  public void UICallback(int choiceNum)
  {
    ResolveChoice(choiceNum);
  }

  private void ResolveChoice(int choice)
  {
    ToggleChoiceObjects(false);
    PTGameStateManager.TriggerEvent(currChoiceSet.choices[choice].triggeredEvent);

    //  If there's no dialogue to be triggered, player regains control and we don't start
    //  any sort of dialogue
    if (currChoiceSet.choices[choice].dialogue == null ||
      currChoiceSet.choices[choice].dialogue.set == null ||
      currChoiceSet.choices[choice].dialogue.set.Count == 0)
    {
      playerRef.TogglePlayerControl(true);
    }
    else
    {
      StartDialogue(currChoiceSet.choices[choice].dialogue);
    }
  }
}

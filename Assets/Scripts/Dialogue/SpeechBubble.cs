﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[Serializable]
public class DialogueBeat
{
  public string dialogue;
  public float time = 0.0f;
  [HideInInspector]
  public bool said = false;

  public DialogueBeat(DialogueBeat other)
  {
    dialogue = other.dialogue;
    time = other.time;
  }
}

public class SpeechBubble : MonoBehaviour
{
  public string speakerName = "";
  public float heightMargin = 0.15f;
  public SpriteRenderer speechBubble;
  public RectTransform textMeshTransform;
  public TextMeshPro textMesh;
  public MeshRenderer textMeshRenderer;

  private List<DialogueBeat> beats;
  private float remainingDialogueTime = -1.0f;

  public void Awake()
  {
    SetVisibility(false);
    textMeshRenderer.sortingLayerName = "SpeechBubbleText";
  }

  public void Update()
  {
    if (remainingDialogueTime > 0.0f)
    {
      remainingDialogueTime -= Time.deltaTime;
      if (remainingDialogueTime <= 0.0f)
      {
        if (beats == null || beats.Count == 0)
        {
          remainingDialogueTime = -1.0f;
          SetVisibility(false);
          return;
        }

        InsertDialogueFromSequence();
      }
    }
  }

  public void SetVisibility(bool set)
  {
    speechBubble.enabled = set;
    textMeshRenderer.enabled = set;
  }

  public void SetDialogue(string dialogue)
  {
    Reset();
    SetText(dialogue);
    SetVisibility(true);
    remainingDialogueTime = -1.0f;
  }

  public void SetTimedDialogue(string dialogue, float time)
  {
    Reset();
    SetTimedDialogueImpl(dialogue, time);
  }

  public void SetTimedDialogueSequence(List<DialogueBeat> dialogue)
  {
    Reset();
    beats = dialogue.ConvertAll(d => new DialogueBeat(d));
    InsertDialogueFromSequence();
  }

  //  Private helper functions

  private void InsertDialogueFromSequence()
  {
    if (beats.Count == 0)
    {
      Debug.LogError("Trying to insert dialogue from empty sequence");
      return;
    }

    SetTimedDialogueImpl(beats[0].dialogue, beats[0].time);
    beats.RemoveAt(0);
  }

  private void SetTimedDialogueImpl(string dialogue, float time)
  {
    SetText(dialogue);
    SetVisibility(true);
    remainingDialogueTime = time;
  }

  private void SetText(string text)
  {
    textMesh.text = text;
    FixSize();
  }

  private void FixSize()
  {
    Vector2 prefVals = textMesh.GetPreferredValues();
    speechBubble.size = new Vector2(speechBubble.size.x, prefVals.y + heightMargin);
    textMeshTransform.sizeDelta = new Vector2(textMeshTransform.sizeDelta.x, prefVals.y);
  }

  private void Reset()
  {
    beats = null;
    remainingDialogueTime = -1.0f;
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTPlayerController : MonoBehaviour
{
  public enum PlayerState
  {
    Idle,
    Walking,
    Interacting,
    NoControl,
  }

  public float movementSpeed = 1.0f;
  public float mouseInactivityThreshold = 0.2f;

  [Tooltip("Reference to child GameObject that has player's SpriteRenderer component")]
  public SpriteRenderer spriteObject;

  public Animator animator;
  public AudioSource footstepAudio;

  //  Private references for code
  private SpriteRenderer spriteRenderer;
  private PTInteractable lastInteractable;

  public float timeSinceMouseMoved = 0.0f;

  public PlayerState playerState = PlayerState.Idle;
  public PlayerState prevPlayerState = PlayerState.Idle;

  private Vector2 currMousePos;
  public bool playerHasControl = true;

  private bool skipFrame = false;

  public float canInteractDelay = 0.5f;
  private float regainControlTimer = 0.0f;

  public Collider2D bodyCollider;
  public Rigidbody2D body;

  //  For external sources to be able to enable/disable player input
  public void TogglePlayerControl(bool set)
  {
    playerHasControl = set;
    skipFrame = true;

    if (set)
    {
      regainControlTimer = canInteractDelay;
      playerState = PlayerState.Idle;
    }
    else
      playerState = PlayerState.NoControl;
  }

  //  For external sources to be able to move the player character
  public void MovePlayer(Vector3 direction)
  {
    //  HARDCODED when moved externally, moves at half speed
    //animator.SetBool("IsWalking", true);
    playerState = PlayerState.Walking;
    transform.Translate(direction * movementSpeed * 0.5f * Time.deltaTime);
    FaceSpriteTowardsPosition(transform.position + direction);
  }

  private bool CheckIfMouseMoved()
  {
    Vector2 prev = currMousePos;
    currMousePos = Input.mousePosition;
    return !currMousePos.Equals(prev);
  }

  private void FaceMouseDirection()
  {
    FaceSpriteTowardsPosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
  }

  private void FaceSpriteTowardsPosition(Vector3 tpos)
  {
    //  I have no idea why this works (why tf is it spos - mpos instead of mpos - spos?)
    //  https://forum.unity.com/threads/2d-sprite-look-at-mouse.211601/
    Vector3 spos = spriteObject.transform.position;
    Quaternion rot = Quaternion.LookRotation(Vector3.forward, tpos - spos);
    spriteObject.transform.rotation = rot;
    spriteObject.transform.eulerAngles = new Vector3(0, 0, spriteObject.transform.eulerAngles.z);
  }

  private void Move(bool lookTowardsMovementDir)
  {
    Vector3 totalMovement = Vector3.zero;
    totalMovement += Vector3.up * Input.GetAxisRaw("Vertical");
    totalMovement += Vector3.right * Input.GetAxisRaw("Horizontal");

    if (totalMovement == Vector3.zero)
    {
      playerState = PlayerState.Idle;
      return;
    }
    playerState = PlayerState.Walking;

    transform.Translate(totalMovement.normalized * movementSpeed * Time.deltaTime);
    //Vector2 movementDir = totalMovement.normalized;
    //body.MovePosition(body.position + movementDir * movementSpeed * Time.deltaTime);

    if (lookTowardsMovementDir)
    {
      FaceSpriteTowardsPosition(transform.position + totalMovement);
    }
  }

  private void Awake()
  {
    spriteRenderer = GetComponent<SpriteRenderer>();
  }

  private void Update()
  {
    if (Input.GetKeyDown(KeyCode.Alpha5))
    {
      bool colliderEnabled = !bodyCollider.enabled;
      if (colliderEnabled)
        DebugPrinter.inst.PrintDebugText("CHEAT: Enabled player collider");
      else
        DebugPrinter.inst.PrintDebugText("CHEAT: Disabled player collider");
      bodyCollider.enabled = colliderEnabled;
    }

    if (regainControlTimer > 0)
      regainControlTimer -= Time.deltaTime;

    if (CheckIfMouseMoved())
      timeSinceMouseMoved = 0.0f;
    else
      timeSinceMouseMoved += Time.deltaTime;

    if (!playerHasControl)
      return;

    if (skipFrame)
    {
      skipFrame = false;
      return;
    }

    if (Input.GetButtonDown("Interact"))
    {
      Interact();
      return;
    }

    bool mouseIsActive = timeSinceMouseMoved < mouseInactivityThreshold;
    Move(!mouseIsActive);
    if (mouseIsActive)
      FaceMouseDirection();
  }

  private void LateUpdate()
  {
    HandleStateChange();
    prevPlayerState = playerState;
  }

  //  Should only be called AT THE END OF UPDATE (or in LateUpdate maybe)
  //  Performs state changes behaviour (ie. setting animator/audio), based on
  //  previous and current state, so it should be called after all actions 
  //  that can change state
  private void HandleStateChange()
  {
    if (prevPlayerState == PlayerState.Walking)
    {
      if (playerState != PlayerState.Walking)
      {
        animator.SetBool("IsWalking", false);
        footstepAudio.Stop();
      }
    }

    if (playerState == PlayerState.Walking)
    {
      if (prevPlayerState != PlayerState.Walking)
      {
        animator.SetBool("IsWalking", true);
        footstepAudio.Play();
      }
    }
  }

  private void Interact()
  {
    if (regainControlTimer > 0)
    {
      Debug.LogFormat("Player must wait {0} seconds before being able to interact again", regainControlTimer);
      return;
    }

    if (lastInteractable != null)
    {
      if (lastInteractable.Interact(this))
      {
        Debug.Log("Successful interaction");
      }
      else
      {
        Debug.Log("Unsuccessful interaction");
      }
    }
  }

  public void OnTriggerEnter2D(Collider2D other)
  {
    PTInteractable has = other.GetComponent<PTInteractable>();
    if (has != null)
      lastInteractable = has;
  }

  public void OnTriggerExit2D(Collider2D other)
  {
    PTInteractable has = other.GetComponent<PTInteractable>();
    if (has == lastInteractable)
      lastInteractable = null;
  }
}

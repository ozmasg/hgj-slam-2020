﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTZoneLight : MonoBehaviour
{
  private enum ZoneState
  {
    PlayerLocationUnknown,
    PlayerInExterior,
    PlayerInInterior,
  }

  public SpriteRenderer roofSprite;
  public GameObject doorOpenLight;
  public GameObject doorClosedLight;

  public PTPlayerTrackCollider exteriorCollider;
  public PTPlayerTrackCollider interiorCollider;

  public bool behaveAsLocked = true;

  public float lockedTransparency = 0.9f;
  public float unlockedTransparency = 0.35f;

  private ZoneState prevState = ZoneState.PlayerLocationUnknown;

  public void SetLocked(bool locked)
  {
    if (locked)
    {
      doorOpenLight.SetActive(false);
      doorClosedLight.SetActive(true);
    }
    else
    {
      doorOpenLight.SetActive(true);
      doorClosedLight.SetActive(false);
    }
    behaveAsLocked = locked;
  }

  public void Update()
  {
    bool inExterior = exteriorCollider.playerInside;
    bool inInterior = interiorCollider.playerInside;

    bool playerLocKnown = inExterior || inInterior;

    if (inExterior && prevState == ZoneState.PlayerLocationUnknown)
    {
      //  todo: impl gradual
      Color w = Color.white;
      w.a = behaveAsLocked ? lockedTransparency : unlockedTransparency;
      roofSprite.color = w;
    }
    else if (!playerLocKnown && prevState == ZoneState.PlayerInExterior)
    {
      roofSprite.color = Color.white;
    }

    //  Handle states
    if (!playerLocKnown && prevState == ZoneState.PlayerInExterior)
    {
      prevState = ZoneState.PlayerLocationUnknown;
    }
    else if (inExterior)
    {
      prevState = ZoneState.PlayerInExterior;
    }
    else if (inInterior)
    {
      prevState = ZoneState.PlayerInInterior;
    }
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTDoor : PTInteractable
{
  public bool locked = true;
  public string requiredItemName;
  public Collider2D doorCollider;

  public PTZoneLight zoneLight;

  public override bool Interact(PTPlayerController player)
  {
    bool playerHasItem = GameState.PlayerHasItem(requiredItemName);
    if (playerHasItem)
    {
      doorCollider.enabled = false;
      return true;
    }
    return false;
  }

  public void Toggle(bool set)
  {
    doorCollider.enabled = set;
    zoneLight.behaveAsLocked = set;
    zoneLight.SetLocked(set);
    locked = set;
  }

  public void OnTriggerEnter2D(Collider2D other)
  {
    if (locked && other.GetComponent<PTPlayerController>() != null)
    {
      PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.lockedDoor, 1.5f);
    }
  }
}

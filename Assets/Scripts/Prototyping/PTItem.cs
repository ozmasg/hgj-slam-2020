﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTItem : PTInteractable
{
  public string itemName;

  public override bool Interact(PTPlayerController player)
  {
    GameState.GivePlayerItem(itemName);
    gameObject.SetActive(false);
    return true;
  }
}

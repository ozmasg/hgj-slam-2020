﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PTMenu : MonoBehaviour
{
  public Image blackSplash;
  public GameObject mainMenuCamera;
  public GameObject mainMenuRef;
  public GameObject creditsRef;

  public GraphicRaycaster raycaster;

  public GameObject playerCamera;
  public PTPlayerController playerController;
  public PTBuddyAI buddy;

  private bool starting = false;

  public void Awake()
  {
    mainMenuCamera.SetActive(true);
    playerCamera.SetActive(false);

    playerController.TogglePlayerControl(false);
    buddy.disableBuddyAI = true;
  }

  public void OpenCredits()
  {
    if (!starting)
    {
      mainMenuRef.SetActive(false);
      creditsRef.SetActive(true);
    }
  }

  public void ReturnToMain()
  {
    if (!starting)
    {
      creditsRef.SetActive(false);
      mainMenuRef.SetActive(true);
    }
  }

  public void StartGame()
  {
    if (!starting)
    {
      starting = true;
      StartCoroutine(StartGameSequence());
    }
  }

  IEnumerator StartGameSequence()
  {
    yield return new WaitForSeconds(0.5f);

    float fadeTime = 3.0f;
    while (fadeTime > 0)
    {
      Color b = Color.black;
      b.a = 1.0f - fadeTime / 3.0f;
      blackSplash.color = b;
      fadeTime -= Time.deltaTime;
      yield return null;
    }

    yield return new WaitForSeconds(0.5f);

    playerCamera.SetActive(true);
    mainMenuCamera.SetActive(false);
    playerController.TogglePlayerControl(true);
    buddy.disableBuddyAI = false;
    raycaster.enabled = false;
    PTGameStateManager.inst.StartGame();
  }
}

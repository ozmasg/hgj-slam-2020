﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTToiletEntry : MonoBehaviour
{
  private bool entered = false;
  private bool exited = false;

  public void OnTriggerEnter2D(Collider2D other)
  {
    if (entered)
      return;

    if (other.GetComponent<PTPlayerController>())
    {
      Debug.Log("ENTER");
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoEnteringToilet);
      entered = true;
    }
  }

  public void OnTriggerExit2D(Collider2D other)
  {
    if (exited)
      return;

    if (other.GetComponent<PTPlayerController>())
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoExitingToilet);
      exited = true;
    }
  }
}

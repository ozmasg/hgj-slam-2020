﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTSentry1Character : MonoBehaviour, ISentrySpeaker
{
  public GameObject spriteObject;
  public List<int> loopsSpokenTo = new List<int>();

  public bool Interact(PTSentryAI sentry, PTPlayerController player)
  {
    int currLoop = PTGameStateManager.GetCurrentLoop();
    bool spokenToInCurrLoop = loopsSpokenTo.Contains(currLoop);

    PTGameStateManager.SpeakToSentry1();

    switch (currLoop)
    {
      case 0:
        if (!spokenToInCurrLoop)
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop0Sentry1Choice);
        else if (GameState.buddyStartedGoingToilet)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Sentry1AfterBuddyGoesToilet);
        else if (GameState.choseEngName)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Sentry1EngNameDSet);
        else if (GameState.choseChiName)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Sentry1ChiNameDSet);
        break;

      case 1:
        if (!spokenToInCurrLoop)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry1Init);
          break;
        }

        bool playerHasRT = GameState.PlayerHasItem(GameData.Items.redTape);
        bool playerHasWT = GameState.PlayerHasItem(GameData.Items.whiteTape);

        if (GameState.usedRedTape)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry1AfterTyingRT);
        else if (GameState.usedWhiteTape)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry1AfterTyingWT);
        else
        {
          if (playerHasRT && playerHasWT)
            DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry1HasRTHasWT);
          else if (playerHasRT)
            DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry1HasRT);
          else if (playerHasWT)
            DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry1HasWT);
          else
            DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry1Init);
        }
        break;

      case 2:
        if (!spokenToInCurrLoop)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry1Init);
          break;
        }

        if (GameState.usedFoodOnRedPlate)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry1TalkAboutFoodOnRedPlate);
        else if (GameState.usedFoodOnWhitePlate)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry1TalkAboutFoodOnWhitePlate);
        else if (GameState.PlayerHasItem(GameData.Items.food))
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry1TalkAboutFood);
        else
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry1Init);
        break;

      case 3:
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop3Sentry1Init);
        break;
    }

    loopsSpokenTo.Add(currLoop);
    return true;
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PTGameStateManager : MonoBehaviour
{
  public string prologueText = "TEKONG ISLAND, NATIONAL SERVICE GUARD DUTY";

  public GameObject playerRef;
  public GameObject buddyRef;
  public GameObject sentry1Ref;
  public GameObject sentry2Ref;
  public GameObject sgtRef;

  public GameObject signingBookRef;

  public GameObject loop2BunkEntryTriggerRef;
  public GameObject loop3BunkEntryTriggerRef;

  public Transform GEBuddyRef;
  public Transform GESentry1Ref;
  public Transform GESentry2Ref;

  public Transform BEBuddyRef;
  public Transform BESgtRef;
  public Transform BESentry1Ref;
  public Transform BESentry2Ref;

  public Image invWhiteTapeRef;
  public Image invRedTapeRef;
  public Image invFoodRef;

  public PTDoor toiletDoorRef;
  public PTDoor armouryDoorRef;
  public PTDoor bunkDoorRef;

  public PTBananaTree bananaTreeRef;
  public PTPlates plateRef;

  public PTItem whiteTapeRef;
  public PTItem redTapeRef;
  public PTItem foodRef;

  public Image blackSplashImg;
  public Text blackSplashTxt;
  public Text surveyTxt;

  public static PTGameStateManager inst { get; private set; }

  private static int currentLoop = 0;

  public void Awake()
  {
    inst = this;
    currentLoop = 0;

    Debug.Log("Resetting global values");
    GameState.Reset();
  }

  public void Start()
  {

  }

  public void StartGame()
  {
    StartCoroutine(OpeningSplash());
  }

  public void OnDestroy()
  {
    inst = null;
  }

  public void Update()
  {
    if (Input.GetKeyDown(KeyCode.Alpha8))
    {
      Debug.Log("CHEAT ACTIVATED: TOGGLING TAPE TRIGGERS");
      if (GameState.usedWhiteTape)
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED WHITE TAPE = false, USED RED TAPE = true");
        GameState.usedWhiteTape = false;
        GameState.usedRedTape = true;
      }
      else if (GameState.usedRedTape)
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED RED TAPE = false, USED WHITE TAPE = true");
        GameState.usedWhiteTape = true;
        GameState.usedRedTape = false;
      }
      else
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED RED TAPE = false, USED WHITE TAPE = true");
        GameState.usedWhiteTape = true;
        GameState.usedRedTape = false;
      }
    }

    if (Input.GetKeyDown(KeyCode.Alpha9))
    {
      Debug.Log("CHEAT ACTIVATED: TOGGLING FOOD TRIGGERS");
      if (GameState.usedFoodOnRedPlate)
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED FOOD ON RED PLATE = true, USED FOOD ON WHITE PLATE = false");
        GameState.usedFoodOnRedPlate = true;
        GameState.usedFoodOnWhitePlate = false;
      }
      else if (GameState.usedFoodOnWhitePlate)
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED FOOD ON WHITE PLATE = true, USED FOOD ON RED PLATE = false");
        GameState.usedFoodOnRedPlate = false;
        GameState.usedFoodOnWhitePlate = true;
      }
      else
      {
        DebugPrinter.inst.PrintDebugText("CHEAT: Set USED FOOD ON RED PLATE = true, USED FOOD ON WHITE PLATE = false");
        GameState.usedFoodOnRedPlate = true;
        GameState.usedFoodOnWhitePlate = false;
      }
    }

    if (Input.GetKeyDown(KeyCode.Alpha0))
    {
      DebugPrinter.inst.PrintDebugText("CHEAT: ADVANCING LOOP");
      CheatAdvanceLoop();
    }
  }

  public static int GetCurrentLoop()
  {
    return currentLoop;
  }

  public static void SpeakToSentry1()
  {
    GameState.spokenToSentry1.Add(currentLoop);
  }

  public static void SpeakToSentry2()
  {
    GameState.spokenToSentry2.Add(currentLoop);
  }

  public void ResolveEndgame()
  {
    //if (GameState.usedWhiteTape)
    //{
    buddyRef.SetActive(true);
    buddyRef.transform.position = GEBuddyRef.position;
    buddyRef.GetComponent<PTBuddyAI>().spriteObject.gameObject.transform.rotation = GEBuddyRef.rotation;
    buddyRef.GetComponent<PTBuddyAI>().disableBuddyAI = true;

    sentry1Ref.SetActive(true);
    sentry1Ref.transform.position = GESentry1Ref.position;
    sentry1Ref.GetComponent<PTSentryAI>().spriteObject.transform.rotation = GESentry1Ref.rotation;

    sentry2Ref.SetActive(true);
    sentry2Ref.transform.position = GESentry2Ref.position;
    sentry2Ref.GetComponent<PTSentryAI>().spriteObject.transform.rotation = GESentry2Ref.rotation;

    DialogueSystem.inst.StartDialogue(GameData.Dialogue.goodEndDialogue);
    //}
    //else
    //{
    //  buddyRef.SetActive(true);
    //  buddyRef.transform.position = BEBuddyRef.position;
    //  buddyRef.GetComponent<PTBuddyAI>().spriteObject.gameObject.transform.rotation = BEBuddyRef.rotation;
    //  buddyRef.GetComponent<PTBuddyAI>().disableBuddyAI = true;

    //  sentry1Ref.SetActive(true);
    //  sentry1Ref.transform.position = BESentry1Ref.position;
    //  sentry1Ref.GetComponent<PTSentryAI>().spriteObject.transform.rotation = BESentry1Ref.rotation;

    //  sentry2Ref.SetActive(true);
    //  sentry2Ref.transform.position = BESentry2Ref.position;
    //  sentry2Ref.GetComponent<PTSentryAI>().spriteObject.transform.rotation = BESentry2Ref.rotation;

    //  sgtRef.SetActive(true);
    //  sgtRef.transform.position = BESgtRef.position;
    //  sgtRef.GetComponent<PTSentryAI>().spriteObject.transform.rotation = BESgtRef.rotation;

    //  DialogueSystem.inst.StartDialogue(GameData.Dialogue.badEndDialogue);
    //}
  }

  public void CloseGame()
  {
    playerRef.GetComponent<PTPlayerController>().TogglePlayerControl(false);
    StartCoroutine(WaitToShowEndSplash());
  }

  IEnumerator WaitToShowEndSplash()
  {
    yield return new WaitForSeconds(0.5f);

    float fadeInTime = 0.5f;
    float stillTime = 3.0f;
    float stillFadeOutTime = 0.5f;
    float stillFadeInTime = 0.5f;
    float fadeOutTime = 1.0f;

    float fadeInCounter = 1.5f;
    float stillCounter = stillTime;
    float fadeOutCounter = fadeOutTime;

    yield return new WaitForSeconds(1.5f);
    blackSplashImg.color = Color.black;
    blackSplashTxt.text = "Randall's shift ended at 2 a.m.";
    blackSplashTxt.color = Color.white;

    while (fadeInCounter > 0.0f)
    {
      Color b = Color.black;
      Color tc = blackSplashTxt.color;
      b.a = 1.0f - fadeInCounter / fadeInTime;
      tc.a = 1.0f - fadeInCounter / fadeInTime;
      blackSplashImg.color = b;
      blackSplashTxt.color = tc;
      fadeInCounter -= Time.deltaTime;
      yield return null;
    }

    yield return new WaitForSeconds(stillTime);

    //  fade out the text
    stillCounter = stillFadeOutTime;
    while (stillCounter >= 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - (1.0f - stillCounter / stillFadeOutTime);
      blackSplashTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    blackSplashTxt.text = "The stories he was told, however, kept him awake until 6 a.m.";

    //  fade in the text
    stillCounter = stillFadeInTime;
    while (stillCounter > 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - stillCounter / stillFadeInTime;
      blackSplashTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    //  pause
    yield return new WaitForSeconds(stillTime);

    //  fade out the text
    stillCounter = stillFadeOutTime;
    while (stillCounter >= 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - (1.0f - stillCounter / stillFadeOutTime);
      blackSplashTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    blackSplashTxt.text = "Thanks for playing!";

    //  fade in the text
    stillCounter = stillFadeInTime;
    while (stillCounter > 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - stillCounter / stillFadeInTime;
      blackSplashTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    yield return new WaitForSeconds(1.5f);

    stillCounter = stillFadeInTime;
    while (stillCounter > 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - stillCounter / stillFadeInTime;
      surveyTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }
  }

  public static void TriggerEvent(GameData.Events eventEnum)
  {
    if (eventEnum == GameData.Events.NoEvent)
      return;

    Debug.LogFormat("TriggerEvent for enum {0}", eventEnum.ToString());

    switch (eventEnum)
    {
      case GameData.Events.Endgame:
        inst.ResolveEndgame();
        break;

      case GameData.Events.Closegame:
        inst.CloseGame();
        break;

      case GameData.Events.AdvanceLoop:
        inst.AdvanceLoop();
        break;

      case GameData.Events.ChooseChineseName:
        GameState.choseChiName = true;
        break;

      case GameData.Events.ChooseEnglishName:
        GameState.choseEngName = true;
        break;

      case GameData.Events.BuddyGoingToToilet:
        GameState.buddyStartedGoingToilet = true;
        inst.buddyRef.GetComponent<PTBuddyAI>().StartGoingToToilet();
        break;

      case GameData.Events.PlayerEnteredBunk:
        GameState.playerTriggeredBunkMonologue = true;
        inst.sgtRef.SetActive(false);
        break;

      case GameData.Events.PlayerEnteredToilet:
        GameState.playerHasGoneToToilet = true;
        break;

      case GameData.Events.PickedUpRedTape:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.getTape);
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPickupRedTape);
        inst.invRedTapeRef.color = Color.white;
        break;

      case GameData.Events.PickedUpWhiteTape:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.getTape);
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPickupWhiteTape);
        inst.invWhiteTapeRef.color = Color.white;
        break;

      case GameData.Events.PickedUpFood:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.pickupFood);
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoPickupFood);
        inst.invFoodRef.color = Color.white;
        break;

      case GameData.Events.UseWhiteTapeOnBananaTree:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.placeTapeOnTree);
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPlaceWhiteTape);
        inst.bananaTreeRef.used = true;
        GameState.usedWhiteTape = true;
        inst.whiteTapeRef.gameObject.SetActive(false);
        inst.redTapeRef.gameObject.SetActive(false);
        inst.bananaTreeRef.GetComponent<PTBananaTree>().UpdateColour();
        inst.invWhiteTapeRef.color = Color.grey;
        break;

      case GameData.Events.UseRedTapeOnBananaTree:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.placeTapeOnTree);
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPlaceRedTape);
        inst.bananaTreeRef.used = true;
        GameState.usedRedTape = true;
        inst.whiteTapeRef.gameObject.SetActive(false);
        inst.redTapeRef.gameObject.SetActive(false);
        inst.bananaTreeRef.GetComponent<PTBananaTree>().UpdateColour();
        inst.invRedTapeRef.color = Color.grey;
        break;

      case GameData.Events.PlaceFoodOnRedPlate:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.placeFoodOnPlate);
        inst.plateRef.used = true;
        inst.plateRef.GetComponent<PTPlates>().PlaceFood(true);
        GameState.hasFood = false;
        GameState.usedFoodOnRedPlate = true;
        inst.invFoodRef.color = Color.grey;
        break;

      case GameData.Events.PlaceFoodOnWhitePlate:
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.placeFoodOnPlate);
        inst.plateRef.used = true;
        inst.plateRef.GetComponent<PTPlates>().PlaceFood(false);
        GameState.hasFood = false;
        GameState.usedFoodOnWhitePlate = true;
        inst.invFoodRef.color = Color.grey;
        break;

      case GameData.Events.SpeakToSgtInLoop1AfterSpeakToSentry2:
        GameState.GivePlayerItem(GameData.Items.whiteTape);
        break;

      case GameData.Events.SpeakToSentry2InLoop1:
        inst.bananaTreeRef.gameObject.SetActive(true);
        break;

      case GameData.Events.SpeakToSentry1InLoop2:
        inst.plateRef.gameObject.SetActive(true);
        break;

      case GameData.Events.SpeakToBuddy:
        inst.buddyRef.GetComponent<PTBuddyAI>().CompleteStaticDialogue();
        break;
    };
  }

  private void CheatAdvanceLoop()
  {
    //  just used to set variables that should be set
    switch (currentLoop)
    {
      case 0:
        GameState.buddyStartedGoingToilet = true;
        SpeakToSentry1();
        SpeakToSentry2();
        break;

      case 1:
        GameState.playerHasGoneToToilet = true;
        SpeakToSentry1();
        SpeakToSentry2();
        break;

      case 2:
        GameState.playerTriggeredBunkMonologue = true;
        inst.sgtRef.SetActive(false);
        SpeakToSentry1();
        SpeakToSentry2();
        break;
    }

    AdvanceLoop();
  }

  public void AdvanceLoop()
  {
    switch (currentLoop)
    {
      case 0:
        toiletDoorRef.Toggle(false);
        buddyRef.SetActive(false);
        redTapeRef.gameObject.SetActive(true);
        break;

      case 1:
        bunkDoorRef.Toggle(false);
        foodRef.gameObject.SetActive(true);
        redTapeRef.gameObject.SetActive(false);

        if (!GameState.usedRedTape && !GameState.usedWhiteTape)
          bananaTreeRef.gameObject.SetActive(false);

        break;

      case 2:
        if (!GameState.usedWhiteTape)
          sentry2Ref.SetActive(false);
        if (!GameState.usedFoodOnRedPlate)
          sentry1Ref.SetActive(false);

        if (!GameState.usedFoodOnRedPlate && !GameState.usedFoodOnWhitePlate)
          plateRef.gameObject.SetActive(false);

        foodRef.gameObject.SetActive(false);
        loop2BunkEntryTriggerRef.gameObject.SetActive(false);
        loop3BunkEntryTriggerRef.gameObject.SetActive(true);

        signingBookRef.SetActive(false);
        break;
    }

    PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.signBook, 2.0f);
    PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.timePasses);

    StartCoroutine(LoopTransition(currentLoop, currentLoop + 1));
    ++currentLoop;
  }

  //  Checks for end-of-loop conditions and triggers respective dialogues
  public void ResolveInteractionWithSigningBook()
  {
    switch (currentLoop)
    {
      case 0:
        if (!GameState.buddyStartedGoingToilet)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0SgtHasntGoneToilet);
        }
        else
        {
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop0SignBookChoice);
        }
        break;

      case 1:
        if (!GameState.playerHasGoneToToilet)
        {
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1SignBookChoiceNotReady);
        }
        else if (
          !GameState.HasSpokenToSentryInLoop(1, 1) ||
          !GameState.HasSpokenToSentryInLoop(2, 1))
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1SgtPlayerHasntGoneToilet);
        }
        else
        {
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1SignBookChoiceReady);
        }
        break;

      case 2:
        if (!GameState.playerTriggeredBunkMonologue)
        {
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop2SignBookChoiceNotReady);
        }
        else if (
          !GameState.HasSpokenToSentryInLoop(1, 2) ||
          !GameState.HasSpokenToSentryInLoop(2, 2))
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoSignBookBeforeTalkingToSentries);
        }
        else
        {
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop2SignBookChoiceReady);
        }
        break;

      case 3:
        Debug.LogWarning("Should not be possible to sign book in loop 3!");
        break;
    }
  }

  public IEnumerator OpeningSplash()
  {
    PTPlayerController player = playerRef.GetComponent<PTPlayerController>();
    player.TogglePlayerControl(false);

    blackSplashImg.color = Color.black;
    blackSplashTxt.text = prologueText;
    blackSplashTxt.color = Color.white;

    float fadeInTime = 3.0f;
    float fadeOutTime = 0.5f;
    float stillTime = 2.0f;

    float fadeCounter = fadeInTime;

    while (fadeCounter > 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - fadeCounter / fadeInTime;
      blackSplashTxt.color = tc;
      fadeCounter -= Time.deltaTime;
      yield return null;
    }

    yield return new WaitForSeconds(stillTime);

    fadeCounter = fadeOutTime;
    while (fadeCounter >= 0.0f)
    {
      Color b = Color.black;
      Color tc = blackSplashTxt.color;
      b.a = 1.0f - (1.0f - fadeCounter / fadeOutTime);
      tc.a = 1.0f - (1.0f - fadeCounter / fadeOutTime);
      blackSplashTxt.color = tc;
      fadeCounter -= Time.deltaTime;
      yield return null;
    }

    StartCoroutine(LoopTransition(-1, -1));
  }

  public IEnumerator LoopTransition(int currLoop, int nextLoop)
  {
    PTPlayerController player = playerRef.GetComponent<PTPlayerController>();
    player.TogglePlayerControl(false);

    string currTime = "";
    string nextTime = "";
    switch (currLoop)
    {
      case -1: currTime = "00:00"; nextTime = "00:00"; break;
      case 0: currTime = "00:00"; nextTime = "00:30"; break;
      case 1: currTime = "00:30"; nextTime = "01:00"; break;
      case 2: currTime = "01:00"; nextTime = "01:30"; break;
    }
    blackSplashTxt.text = currTime;

    float fadeInTime = 0.5f;
    float stillTime = 1.0f;
    float stillFadeOutTime = 0.5f;
    float stillFadeInTime = 0.5f;
    float still2Time = 1.5f;
    float fadeOutTime = 1.0f;

    float fadeInCounter = fadeInTime;
    float stillCounter = stillTime;
    float fadeOutCounter = fadeOutTime;

    if (currTime != nextTime)
    {
      while (fadeInCounter > 0.0f)
      {
        Color b = Color.black;
        Color tc = blackSplashTxt.color;
        b.a = 1.0f - fadeInCounter / fadeInTime;
        tc.a = 1.0f - fadeInCounter / fadeInTime;
        blackSplashImg.color = b;
        blackSplashTxt.color = tc;
        fadeInCounter -= Time.deltaTime;
        yield return null;
      }

      while (stillCounter >= 0.0f)
      {
        stillCounter -= Time.deltaTime;
        yield return null;
      }

      stillCounter = stillFadeOutTime;

      while (stillCounter >= 0.0f)
      {
        Color tc = blackSplashTxt.color;
        tc.a = 1.0f - (1.0f - stillCounter / stillFadeOutTime);
        blackSplashTxt.color = tc;
        stillCounter -= Time.deltaTime;
        yield return null;
      }
    }

    blackSplashTxt.text = nextTime;
    stillCounter = stillFadeInTime;

    while (stillCounter > 0.0f)
    {
      Color tc = blackSplashTxt.color;
      tc.a = 1.0f - stillCounter / stillFadeInTime;
      blackSplashTxt.color = tc;
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    stillCounter = still2Time;
    while (stillCounter >= 0.0f)
    {
      stillCounter -= Time.deltaTime;
      yield return null;
    }

    while (fadeOutCounter >= 0.0f)
    {
      Color b = Color.black;
      Color tc = blackSplashTxt.color;
      b.a = 1.0f - (1.0f - fadeOutCounter / fadeOutTime);
      tc.a = 1.0f - (1.0f - fadeOutCounter / fadeOutTime);
      blackSplashImg.color = b;
      blackSplashTxt.color = tc;
      fadeOutCounter -= Time.deltaTime;
      yield return null;
    }

    Color bn = Color.black;
    bn.a = 0;
    Color btc = blackSplashTxt.color;
    btc.a = 0;
    blackSplashImg.color = bn;
    blackSplashTxt.color = btc;

    player.TogglePlayerControl(true);
  }
}

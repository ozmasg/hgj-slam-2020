﻿using UnityEngine;

public abstract class PTInteractable : MonoBehaviour
{
  public Collider2D interactCollider;

  //  Interact should be called by the player to trigger interactions.
  //  If the returned bool is false, it means interaction did not occur.
  public abstract bool Interact(PTPlayerController player);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTBananaTree : PTInteractable
{
  public SpriteRenderer bananaTreeDefault;
  public SpriteRenderer bananaTreeRed;
  public SpriteRenderer bananaTreeWhite;

  [HideInInspector]
  public bool used = false;

  public void UpdateColour()
  {
    bananaTreeDefault.enabled = false;
    bananaTreeRed.enabled = false;
    bananaTreeWhite.enabled = false;

    if (GameState.usedRedTape)
      bananaTreeRed.enabled = true;
    else if (GameState.usedWhiteTape)
      bananaTreeWhite.enabled = true;
    else
      bananaTreeDefault.enabled = true;
  }

  public override bool Interact(PTPlayerController player)
  {
    if (GameState.usedRedTape)
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPlaceRedTape);
      return true;
    }
    else if (GameState.usedWhiteTape)
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoPlaceWhiteTape);
      return true;
    }

    bool playerHasRedTape = GameState.PlayerHasItem(GameData.Items.redTape);
    bool playerHasWhiteTape = GameState.PlayerHasItem(GameData.Items.whiteTape);

    if (playerHasRedTape && playerHasWhiteTape)
    {
      DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.bananaTreeChoiceHWRT);
      used = true;
    }
    else if (playerHasRedTape)
    {
      DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.bananaTreeChoiceHRT);
      used = true;
    }
    else if (playerHasWhiteTape)
    {
      DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.bananaTreeChoiceHWT);
      used = true;
    }
    else
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1MonoInteractTreeWithoutTape);
    }

    return true;
  }



}

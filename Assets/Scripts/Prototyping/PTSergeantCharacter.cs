﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTSergeantCharacter : MonoBehaviour, ISentrySpeaker
{
  public GameObject spriteObject;
  public List<int> loopsSpokenTo = new List<int>();

  public bool Interact(PTSentryAI sentry, PTPlayerController player)
  {
    int currLoop = PTGameStateManager.GetCurrentLoop();
    bool spokenToInCurrLoop = loopsSpokenTo.Contains(currLoop);

    switch (currLoop)
    {
      case 0:
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0SgtDefault);
        break;

      case 1:
        bool hasWT = GameState.PlayerHasItem(GameData.Items.whiteTape);
        bool usedTape = (GameState.usedWhiteTape || GameState.usedRedTape);

        if (GameState.HasSpokenToSentryInLoop(2, 1)
          && !hasWT
          && !usedTape)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1SgtAfterTalkingToSentry2);
        }
        else if (!GameState.playerHasGoneToToilet)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1SgtInteractBeforePlayerGoToilet);
        }
        else
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1SgtDefault);
        }
        break;

      case 2:
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2SgtDefault);
        break;
    }

    loopsSpokenTo.Add(currLoop);
    return true;
  }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTPlayerTrackCollider : MonoBehaviour
{
  public bool playerInside;

  public void OnTriggerEnter2D(Collider2D other)
  {
    if (other.GetComponent<PTPlayerController>() == null)
      return;
    playerInside = true;
  }

  public void OnTriggerExit2D(Collider2D other)
  {
    if (other.GetComponent<PTPlayerController>() == null)
      return;
    playerInside = false;
  }
}

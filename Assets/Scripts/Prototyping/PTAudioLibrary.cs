﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTAudioLibrary : MonoBehaviour
{
  public static PTAudioLibrary inst { get; private set; }

  public List<AudioSource> audioSources;
  public int lastUsedSource = 0;

  public void Awake()
  {
    inst = this;
  }

  public void OnDestroy()
  {
    inst = null;
  }

  public void PlayAudio(AudioClip c, float customVolume = 1.0f)
  {
    if (c == null)
    {
      Debug.LogWarning("Trying to play null audio, skipping");
      return;
    }

    ++lastUsedSource;
    if (lastUsedSource == audioSources.Count)
      lastUsedSource = 0;

    audioSources[lastUsedSource].PlayOneShot(c, customVolume);
  }

  public AudioClip lockedDoor;
  public AudioClip getTape;
  public AudioClip placeTapeOnTree;
  public AudioClip pickupFood;
  public AudioClip placeFoodOnPlate;
  public AudioClip lightFlicker;
  public AudioClip lightFlickerOff;
  public AudioClip signBook;
  public AudioClip timePasses;
}

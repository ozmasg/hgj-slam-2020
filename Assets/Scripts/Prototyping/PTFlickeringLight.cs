﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PTFlickeringLight : MonoBehaviour
{
  public GameObject lightToFlicker;
  public Light2D lightComponent;

  public int collisions = 0;
  public int flickerFirst = 1;
  public int flickerThreshold = 3;

  public void OnTriggerEnter2D(Collider2D other)
  {
    if (collisions >= flickerThreshold)
      return;

    if (other.GetComponent<PTPlayerController>())
    {
      ++collisions;

      if (collisions == flickerFirst)
      {
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.lightFlicker);
        StartCoroutine(StartFlickering(false));
      }
      else if (collisions >= flickerThreshold)
      {
        PTAudioLibrary.inst.PlayAudio(PTAudioLibrary.inst.lightFlickerOff);
        StartCoroutine(StartFlickering(true));
      }
    }
  }

  IEnumerator StartFlickering(bool turnOff)
  {
    float startIntensity = lightComponent.intensity;

    float timer = 0.15f;

    timer = 0.15f;
    lightToFlicker.SetActive(false);
    while (timer > 0.0f)
    {
      timer -= Time.deltaTime;
      yield return null;
    }

    timer = 0.15f;
    lightToFlicker.SetActive(true);

    while (timer > 0.0f)
    {
      timer -= Time.deltaTime;
      yield return null;
    }

    timer = 0.15f;
    lightToFlicker.SetActive(false);
    while (timer > 0.0f)
    {
      timer -= Time.deltaTime;
      yield return null;
    }

    timer = 0.8f;
    lightToFlicker.SetActive(true);

    while (timer > 0.5f)
    {
      lightComponent.intensity = timer;
      timer -= Time.deltaTime;
      yield return null;
    }

    timer = 0.075f;
    lightToFlicker.SetActive(false);

    while (timer > 0.0f)
    {
      timer -= Time.deltaTime;
      yield return null;
    }

    timer = 0.075f;
    lightToFlicker.SetActive(true);

    while (timer > 0.0f)
    {
      timer -= Time.deltaTime;
      yield return null;
    }

    if (turnOff)
    {
      lightToFlicker.SetActive(false);
    }
    else
    {
      lightComponent.intensity = startIntensity;
    }
    yield return null;
  }

}

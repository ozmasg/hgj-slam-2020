﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BuddyWaypoint
{
  public bool waitForInteractBeforeStarting = false;
  public Transform waypointObject;
  public List<DialogueBeat> dialogue;
}

public class PTBuddyAI : PTInteractable
{
  public bool disableBuddyAI = false;

  [Tooltip("How quickly the buddy walks (consistent speed, walks directly towards waypoints)")]
  public float movementSpeed = 1.0f;
  [Tooltip("How far player can go before 'stop' dialogue will be triggered")]
  public float stopPlayerRadius = 5.0f;
  [Tooltip("How close the player will be forced towards the buddy after exiting the stop player radius")]
  public float safePlayerRadius = 3.0f;

  [Tooltip("Create ordered list of path to Sentry 1")]
  public List<BuddyWaypoint> waypointsToSentry1;
  [Tooltip("Create ordered list of path to Sentry 2")]
  public List<BuddyWaypoint> waypointsToSentry2;
  [Tooltip("Create ordered list of path to Guardhouse (until toilet dialogue)")]
  public List<BuddyWaypoint> waypointsToGuardhouse;
  [Tooltip("Create ordered list of path to the toilet (maybe not needed yet)")]
  public List<BuddyWaypoint> waypointsToToilet;

  public List<string> playerExitRadiusWalkingLines;
  public List<string> playerExitRadiusStandingLines;

  private List<BuddyWaypoint> currentWaypoints;

  public Animator animator;
  public AudioSource footstepAudio;
  public SpeechBubble speechBubble;
  public PTPlayerController playerController;

  [Tooltip("Reference to child GameObject that has buddy's Sprite, used for turning")]
  public SpriteRenderer spriteObject;

  private bool _callingToPlayer = false;
  private bool callingToPlayer
  {
    get { return _callingToPlayer; }
    set
    {
      _callingToPlayer = value;
      playerController.TogglePlayerControl(!value);
    }
  }
  private bool pendingInteraction = true;

  private bool isWalking = false;

  private bool trueWalkState = false;
  private bool prevTrueWalkState = false;


  public bool reachedSentry1 = false;
  public bool reachedSentry2 = false;
  public bool reachedGoingToiletPoint = false;
  public bool reachedToilet = false;

  public bool waitingForToiletTrigger = false;
  public bool goingToToilet = false;

  private bool completedInitialDialogue = false;

  public void Start()
  {
    currentWaypoints = waypointsToSentry1;
  }

  //  Inherited
  public override bool Interact(PTPlayerController player)
  {
    if (isWalking)
      return false;

    if (!completedInitialDialogue)
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Buddy1Initial);
      completedInitialDialogue = true;
      return true;
    }

    if (!reachedSentry2)
    {
      if (GameState.HasSpokenToSentryInLoop(1, 0))
      {
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Buddy1AfterSpeakingToS1);
      }
      else
      {
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Buddy1HasntSpokenToS1);
      }
      return true;
    }
    else if (!reachedGoingToiletPoint)
    {
      if (GameState.HasSpokenToSentryInLoop(2, 0))
      {
        StartWalking(waypointsToGuardhouse);
      }
      else
      {
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Buddy1HasntSpokenToS2);
      }
      return true;
    }

    Debug.LogWarning("Nothing should be possible here");
    return false;
  }

  public void CompleteStaticDialogue()
  {
    if (!reachedSentry1)
    {
      StartWalking(waypointsToSentry1);
    }
    else if (!reachedSentry2)
    {
      StartWalking(waypointsToSentry2);
    }
  }

  //  Monobehaviour
  private void Update()
  {
    if (disableBuddyAI)
      return;

    if (waitingForToiletTrigger)
      return;

    if (!goingToToilet)
    {
      if (callingToPlayer)
      {
        animator.SetBool("IsWalking", false);
        Vector3 playerToMeDir = transform.position - playerController.transform.position;
        playerController.MovePlayer(playerToMeDir.normalized);
        FaceSpriteTowardsPosition(playerController.transform.position);

        float playerToMeDist = (transform.position - playerController.transform.position).magnitude;
        if (playerToMeDist <= safePlayerRadius)
        {
          callingToPlayer = false;
          speechBubble.SetVisibility(false);
        }
        return;
      }

      if (pendingInteraction)
      {
        animator.SetBool("IsWalking", false);
        FaceSpriteTowardsPosition(playerController.transform.position);
        if (IsPlayerOutsideStopRadius())
        {
          callingToPlayer = true;
          speechBubble.SetDialogue(GetExceedStandingLine());
          return;
        }
        return;
      }

      if (isWalking)
      {
        if (IsPlayerOutsideStopRadius())
        {
          animator.SetBool("IsWalking", false);
          callingToPlayer = true;
          speechBubble.SetDialogue(GetExceedWalkingLine());
          FaceSpriteTowardsPosition(playerController.transform.position);
        }
        else
        {
          animator.SetBool("IsWalking", true);
          MoveToNextWaypoint();
        }
      }
    }
    else
    {
      MoveToNextWaypoint();
    }

    trueWalkState = isWalking && !callingToPlayer;

    if (trueWalkState != prevTrueWalkState)
    {
      if (trueWalkState)
      {
        footstepAudio.Play();
      }
      else
      {
        footstepAudio.Stop();
      }
    }
  }

  private void LateUpdate()
  {
    prevTrueWalkState = trueWalkState;
  }

  public void StartGoingToToilet()
  {
    waitingForToiletTrigger = false;
    goingToToilet = true;
    isWalking = true;
    currentWaypoints = waypointsToToilet;
  }

  //  Helper
  private void StartWalking(List<BuddyWaypoint> wp)
  {
    StartWaypointsDialogue();
    currentWaypoints = wp;
    isWalking = true;
    pendingInteraction = false;
  }


  private string GetExceedStandingLine()
  {
    int random = UnityEngine.Random.Range(0, playerExitRadiusStandingLines.Count);
    return playerExitRadiusStandingLines[random];
  }

  private string GetExceedWalkingLine()
  {
    int random = UnityEngine.Random.Range(0, playerExitRadiusWalkingLines.Count);
    return playerExitRadiusWalkingLines[random];
  }

  private bool IsPlayerOutsideStopRadius()
  {
    Vector3 distanceToPlayer = playerController.transform.position - transform.position;
    return distanceToPlayer.magnitude >= stopPlayerRadius;
  }

  private void MoveToNextWaypoint()
  {
    List<BuddyWaypoint> waypoints = currentWaypoints;

    if (waypoints.Count == 0)
    {
      CompletedWaypointsEventCallback();
      isWalking = false;
      return;
    }

    Vector3 distanceToWaypoint = waypoints[0].waypointObject.position - transform.position;
    if (distanceToWaypoint.sqrMagnitude < 0.1f)
    {
      waypoints.RemoveAt(0);
      StartWaypointsDialogue();
      return;
    }

    Vector3 movementDir = distanceToWaypoint.normalized;
    transform.Translate(movementDir * movementSpeed * Time.deltaTime);
    FaceSpriteTowardsPosition(transform.position + movementDir);
  }

  private void StartWaypointsDialogue()
  {
    List<BuddyWaypoint> waypoints = currentWaypoints;
    if (waypoints.Count > 0
      && waypoints[0].dialogue != null
      && waypoints[0].dialogue.Count > 0)
    {
      speechBubble.SetTimedDialogueSequence(waypoints[0].dialogue);
    }
  }

  private void CompletedWaypointsEventCallback()
  {
    if (!reachedSentry1)
    {
      reachedSentry1 = true;
      currentWaypoints = waypointsToSentry2;
      pendingInteraction = true;
    }
    else if (!reachedSentry2)
    {
      reachedSentry2 = true;
      currentWaypoints = waypointsToGuardhouse;
      pendingInteraction = true;
    }
    else if (!reachedGoingToiletPoint)
    {
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Buddy1GoingToilet);
      reachedGoingToiletPoint = true;
    }
    else
    {
      Debug.Log("Buddy has reached toilet and will now disintegrate");
      gameObject.SetActive(false);
    }
  }

  private void FaceSpriteTowardsPosition(Vector3 tpos)
  {
    //  I have no idea why this works (why tf is it spos - mpos instead of mpos - spos?)
    //  https://forum.unity.com/threads/2d-sprite-look-at-mouse.211601/
    Vector3 spos = spriteObject.transform.position;
    Quaternion rot = Quaternion.LookRotation(Vector3.forward, tpos - spos);
    spriteObject.transform.rotation = rot;
    spriteObject.transform.eulerAngles = new Vector3(0, 0, spriteObject.transform.eulerAngles.z);
  }
}

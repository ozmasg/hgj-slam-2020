﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTPlates : PTInteractable
{
  public SpriteRenderer spriteRenderer;
  public Sprite foodOnRed;
  public Sprite foodOnWhite;

  [HideInInspector]
  public bool used = false;

  public override bool Interact(PTPlayerController player)
  {
    if (GameState.usedFoodOnRedPlate)
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoPlaceFoodOnRedPlate);
    else if (GameState.usedFoodOnWhitePlate)
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoPlaceFoodOnWhitePlate);
    else if (!GameState.PlayerHasItem(GameData.Items.food))
      DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoInteractPlateWithoutFood);
    else
      DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.foodWithPlateChoices);

    return true;
  }

  public void PlaceFood(bool isRed)
  {
    spriteRenderer.sprite = isRed ? foodOnRed : foodOnWhite;
  }
}

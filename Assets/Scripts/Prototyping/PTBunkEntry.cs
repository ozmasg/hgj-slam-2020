﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTBunkEntry : MonoBehaviour
{
  public bool endgame = false;
  public bool used = false;

  public void OnTriggerEnter2D(Collider2D other)
  {
    if (used)
      return;

    if (other.GetComponent<PTPlayerController>())
    {
      used = true;
      if (endgame)
      {
        Debug.Log("Triggering endgame");
        PTGameStateManager.TriggerEvent(GameData.Events.Endgame);
      }
      else
      {
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2MonoEnterBunk);
      }
    }
  }
}

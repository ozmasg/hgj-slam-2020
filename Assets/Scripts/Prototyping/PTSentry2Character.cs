﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTSentry2Character : MonoBehaviour, ISentrySpeaker
{
  public GameObject spriteObject;
  public List<int> loopsSpokenTo = new List<int>();

  public bool Interact(PTSentryAI sentry, PTPlayerController player)
  {
    int currLoop = PTGameStateManager.GetCurrentLoop();
    bool spokenToInCurrLoop = loopsSpokenTo.Contains(currLoop);

    PTGameStateManager.SpeakToSentry2();

    switch (currLoop)
    {
      case 0:
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop0Sentry2InitDefault);
        break;

      case 1:
        if (!spokenToInCurrLoop)
        {
          PTGameStateManager.TriggerEvent(GameData.Events.SpeakToSentry2InLoop1);
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry2Init);
          break;
        }

        bool playerHasRT = GameState.PlayerHasItem(GameData.Items.redTape);
        bool playerHasWT = GameState.PlayerHasItem(GameData.Items.whiteTape);

        if (GameState.usedRedTape)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry2AfterTyingRT);
        else if (GameState.usedWhiteTape)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry2AfterTyingWT);
        else if (playerHasRT && playerHasWT)
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry2HasRTHasWT);
        else if (playerHasRT)
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry2HasRT);
        else if (playerHasWT)
          DialogueSystem.inst.StartDialogueChoice(GameData.Dialogue.loop1Sentry2HasWT);
        else
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop1Sentry2Init);
        break;


      case 2:
        if (!spokenToInCurrLoop)
        {
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry2Init);
          break;
        }

        if (GameState.usedFoodOnRedPlate)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry2TalkAboutFoodOnRedPlate);
        else if (GameState.usedFoodOnWhitePlate)
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry2TalkAboutFoodOnWhitePlate);
        else if (GameState.PlayerHasItem(GameData.Items.food))
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry2TalkAboutFood);
        else
          DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop2Sentry2Init);
        break;

      case 3:
        DialogueSystem.inst.StartDialogue(GameData.Dialogue.loop3Sentry2Init);
        break;
    }

    loopsSpokenTo.Add(currLoop);
    return true;
  }
}

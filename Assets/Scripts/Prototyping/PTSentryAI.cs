﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTSentryAI : PTInteractable
{
  public string sentrySpeakerTypeName;
  public SpeechBubble speechBubble;
  public GameObject spriteObject;
  private ISentrySpeaker speaker;

  public void Awake()
  {
    Type speakerType = Type.GetType(sentrySpeakerTypeName);
    if (speakerType == null)
    {
      Debug.LogErrorFormat("Unable to GetType for ISentrySpeaker from name {0}", sentrySpeakerTypeName);
      return;
    }
    speaker = gameObject.AddComponent(speakerType) as ISentrySpeaker;
  }

  public override bool Interact(PTPlayerController player)
  {
    return speaker.Interact(this, player);
  }
}

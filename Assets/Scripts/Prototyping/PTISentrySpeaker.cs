﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISentrySpeaker
{
  bool Interact(PTSentryAI sentry, PTPlayerController player);
}

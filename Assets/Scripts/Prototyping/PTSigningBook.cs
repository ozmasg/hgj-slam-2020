﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PTSigningBook : PTInteractable
{
  public override bool Interact(PTPlayerController player)
  {
    PTGameStateManager.inst.ResolveInteractionWithSigningBook();
    return true;
  }
}

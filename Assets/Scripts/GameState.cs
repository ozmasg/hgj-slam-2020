﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{
  public static bool choseEngName;
  public static bool choseChiName;

  public static bool buddyStartedGoingToilet;
  public static bool playerHasGoneToToilet;

  public static bool playerTriggeredBunkMonologue;

  public static bool hasWhiteTape;
  public static bool usedWhiteTape;

  public static bool hasRedTape;
  public static bool usedRedTape;

  public static bool hasFood;
  public static bool usedFoodOnRedPlate;
  public static bool usedFoodOnWhitePlate;

  public static List<int> spokenToSentry1;
  public static List<int> spokenToSentry2;

  public static void Reset()
  {
    hasWhiteTape = usedWhiteTape = false;
    hasRedTape = usedRedTape = false;
    hasFood = usedFoodOnRedPlate = usedFoodOnWhitePlate = false;
    spokenToSentry1 = new List<int>();
    spokenToSentry2 = new List<int>();
  }

  public static bool HasSpokenToSentryInLoop(int sentryNum, int loopNum)
  {
    switch (sentryNum)
    {
      case 1:
        return spokenToSentry1.Contains(loopNum);
      case 2:
        return spokenToSentry2.Contains(loopNum);
    }
    return false;
  }

  public static bool PlayerHasItem(string itemName)
  {
    switch (itemName)
    {
      case GameData.Items.redTape:
        return hasRedTape && !usedRedTape;

      case GameData.Items.whiteTape:
        return hasWhiteTape && !usedWhiteTape;

      case GameData.Items.food:
        return hasFood && !usedFoodOnRedPlate && !usedFoodOnWhitePlate;

      default:
        return false;
    }
  }

  public static void GivePlayerItem(string itemName)
  {
    switch (itemName)
    {
      case GameData.Items.redTape:
        hasRedTape = true;
        PTGameStateManager.TriggerEvent(GameData.Events.PickedUpRedTape);
        return;

      case GameData.Items.whiteTape:
        hasWhiteTape = true;
        PTGameStateManager.TriggerEvent(GameData.Events.PickedUpWhiteTape);
        return;

      case GameData.Items.food:
        hasFood = true;
        PTGameStateManager.TriggerEvent(GameData.Events.PickedUpFood);
        return;

      default:
        Debug.LogErrorFormat("Failed to find item with name {0}", itemName);
        return;
    }
  }
}
